package org.example;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@ResponseBody
public class TodoController {
    private final List<Todo> todos = List.of(
            new Todo(1, "Wake Up Early"),
            new Todo(2, "Write Code"),
            new Todo(3, "Eat"),
            new Todo(4, "English learning")
    );

    @GetMapping("/todos")
    public String todos() {
        StringBuilder sb = new StringBuilder();
        sb.append("""
                <table border="1">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                </tr>
                </thead>
                <tbody>""");

        todos.forEach(todo -> sb.append("""
                    <tr>
                        <td>%s</td>
                        <td>%s</td>
                    </tr>""".formatted(todo.getId(), todo.getName())));

        sb.append("</tbody></table>");

        return sb.toString();
    }

}
